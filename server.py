import os

from flask import Flask, request, jsonify
from flask_cors import CORS

from bson.objectid import ObjectId

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

try:
    username = os.environ.get('MONGODB_USERNAME')
    password = os.environ.get('MONGODB_PASSWORD')

    uri = f'mongodb://{username}:{password}@mongodb:27017/'

    # Establish a connection to the MongoDB database
    client = MongoClient(uri)
    db = client['bus']
    collection = db['bus_data']
    print("Successfully connected to MongoDB")
except ConnectionFailure as e:
    print("Failed to connect to MongoDB:", e)


app = Flask(__name__)
CORS(app)

# Route to get last tracked location of buses
@app.route('/tracking/last', methods=['GET'])
def get_last_tracked_location():
    bus_data = list(collection.find())
    for data in bus_data:
        data['_id'] = str(data['_id'])
    return jsonify(bus_data)



@app.route('/tracking/add', methods=['POST'])
def update_bus_location():
    data = request.json
    bus_id = data.get('_id')
    new_lat = data.get('lat')
    new_lon = data.get('lon')


    if not (bus_id and new_lat and new_lon):
        return jsonify({'error': 'Missing data. _id, lat, and lon are required.'}), 400

    filter = {'_id': ObjectId(bus_id)}
    
    update = {'$set': {'lat': new_lat, 'lon': new_lon}}
    
    result = collection.update_one(filter, update)
    
    if result.modified_count == 1:
        return jsonify({'success': True, 'message': 'Bus location updated successfully.'})
    else:
        return jsonify({'error': 'Failed to update bus location. Bus not found.'}), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0')
